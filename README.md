# LWC Markdown Editor

A simple markdown editor built in Salesforce using Lightning Web Components.

Read the [blog post](https://marktyrrell.com/posts/build-a-markdown-editor-in-salesforce-using-lightning-web-components/) to learn how it was built.

## Installation
Install the project in a scratch org or developer org using the Salesforce CLI.

### Developer Org
```bash
sfdx force:source:deploy -u <username> -p force-app
```

### Scratch Org
```bash
sfdx force:source:push -u <username>
```

## Issues
If you find any bugs in the code or the blog post don't be afraid to raise a [new issue](https://gitlab.com/marktuk/lwc-markdown-editor/issues/new).

## License
Released under the terms of the [MIT License](LICENSE.md).